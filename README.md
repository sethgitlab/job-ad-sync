# Job Ad Sync

This script takes all markdown files(.md) in the `Job-Ads` folders, and syncronizes them to Greenhouse. 

Job Ads are synchronized based on the meta data they have in their markdown. Any text in these markdown files will overwrite the Greenhouse job post text. 

The following should be added to each job ad markdown file. 

```md
--- 
title: Senior Backend Engineer 
location: "Remote"
post_ids: "555,777,999"
sync: true
---
```

`title`: The title of the job. Optional. If it is omitted, the title in Greenhouse will be used. 

`location`: The location of the job. Optional. If it is omitted, the location in Greenhouse will be used. 

`post_ids`: A comma separated list of Greenhouse job post ids. This is useful in the event one markdown file contains the content for many job posts. In this case, only 1 markdown file is required. To get the job post id, you can edit a job post in Greenhouse and the job id will be in the URL, i.e. https://gitlab.greenhouse.io/jobapps/12345678910/edit, where 12345678910 is the job id. 

`sync`: Set this to true to update Greenhouse. Set it to false and the text in the markdown file will not update Greenhouse. Useful for writing a job ad prior to the Greenhouse post being opened. 

