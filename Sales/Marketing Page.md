--- 
title: Marketing Page
location: "Remote"
post_ids: "9878111"
sync: true
---

Senior Backend Engineer, Secure:Dynamic Analysis (C# or Golang) 

This [Senior Backend Engineer](https://about.gitlab.com/job-families/engineering/backend-engineer/#backend-engineering-roles-at-gitlab) position for our [dynamic analysis testing team](https://about.gitlab.com/handbook/engineering/development/secure/dynamic-analysis/) is 100% remote.

It’s an exciting time to join our team. We're the world’s largest all-remote company, and we've been intentionally building our culture this way from the start. With more than 1,300 team members in 65+ countries, GitLab is a place where you can contribute from almost anywhere. We are an ambitious, productive team that embraces a set of shared [​values](https://about.gitlab.com/handbook/values/)​ in everything we do.

As a Senior Backend Engineer in our [Dynamic Analysis](https://about.gitlab.com/handbook/product/categories/#dynamic-analysis-group) team, you'll play a key role in [maturing](https://about.gitlab.com/direction/maturity/) our fuzz testing offering. Our Fuzz Testing tool is a recent addition to GitLab and your work will impact the direction of this key driver of annual recurring revenue. 

You'll need to be a senior-level engineer who's comfortable working in C# or Golang. You will also need to be comfortable with other programming languages as your work will shape our fuzz testing offering in various languages including C, C++, Java, Rust, Swift, Javascript and more. 

We do not expect mastery of all these languages. We are looking for someone who is excited and knows how to jump into a new languages when the need arises. 

The [culture](https://about.gitlab.com/company/culture/) here at GitLab is something we’re incredibly proud of. Some of the [benefits](https://about.gitlab.com/handbook/total-rewards/benefits/) you’ll be entitled to vary by the region or country you’re in. However, all GitLab team members are fully remote and receive a "no ask, must tell" paid-time-off policy, where we don’t count the number of days you take off annually -- instead, we focus on your results. You can work the hours you choose, enabled by our [asynchronous approach to communication](https://about.gitlab.com/handbook/communication/#asynchronous-communication). You can also expect stock options and a competitive salary. Our compensation calculator will be shared with selected candidates before any interview.

[Diversity, Inclusion, and Belonging](https://about.gitlab.com/company/culture/inclusion/) (DIB) are fundamental to the success of GitLab. We want to infuse DIB in every way possible and in all that we do. We strive to create a transparent environment where all team members around the world feel that their voices are heard and welcomed. We also aim to be a place where people can show up as their full selves each day and contribute their best. With more than 100,000 organizations using GitLab, our goal is to have a team that is representative of our users.

#### What you'll do in this role

* Play a key role in improving the maturity of our fuzz testing tools. 
* Work across a range of languages and frameworks not limited to just C# or Go. Other languages include C, C++, Java, Python, Rust, Swift, Javascript.
* Advocate for improvements to the fuzz testing product's quality, security, and performance.
* Solve technical problems of high scope and complexity.
* Exert influence on the overall objectives and [long-range goals](https://about.gitlab.com/direction/secure/dynamic-analysis/fuzz-testing/) of the Dynamic Analysis Testing group.
* Help to define and improve our internal standards for style, maintainability, and best practices for a high-scale web environment. Maintain and advocate for these standards through code review.
* Represent GitLab and its values in public communication around broader initiatives, specific projects, and community contributions.
* Provide mentorship for Junior and Intermediate Engineers on your team to help them grow in their technical responsibilities and remove blockers to their autonomy.
* Confidently ship moderately sized features and improvements with minimal guidance and support from other team members.
* Collaborate with the team on larger projects.
* Improve the engineering projects at GitLab via maintainer trainee program at your own comfortable pace, while striving to become a project maintainer.

##### You should apply if you bring:

* Significant professional experience with C# or Go.
* Penchant for learning multiple programming languages. 
* Proficiency in the English language, both written and verbal, sufficient for success in a remote and largely asynchronous work environment
* Demonstrated capacity to clearly and concisely communicate about complex technical, architectural, and/or organizational problems and propose thorough iterative solutions
* Experience with performance and optimization problems and a demonstrated ability to both diagnose and prevent these problems
* Comfort working in a highly agile, intensely iterative software development process
* Demonstrated ability to onboard and integrate with an organization long-term
* Positive and solution-oriented mindset
* Effective communication skills: Regularly achieve consensus with peers, and clear status updates
* An inclination towards communication, inclusion, and visibility
* Experience owning a project from concept to production, including proposal, discussion, and execution.
* Self-motivated and self-managing, with strong organizational skills.
* Demonstrated ability to work closely with other parts of the organization
* Share our values, and work in accordance with those values


Also, we know it’s tough, but please try to avoid the ​​confidence gap​.​​ You don’t have to match all the listed requirements exactly to be considered for this role.

Our hiring process for this Senior Backend Engineer, Fuzz Testing position typically follows four stages. The details of this process and our leveling structure can be found [on our job family page](https://about.gitlab.com/job-families/engineering/backend-engineer/#hiring-process).
