#!/usr/bin/env ruby

require 'kramdown'
require 'front_matter_parser'
require 'uri'
require 'base64'
require 'colorize'
require 'json'
require 'net/http'

GH_API_TOKEN = ENV['GH_API_TOKEN']
DRY_RUN = ENV['DRY_RUN']&.downcase == "false" ? false : true

USER_ID = ENV['GH_USER_ID'] # Used for auditing purposes, not authentication. 

REQ_OPTIONS = {
    use_ssl: true
  }

$exit_code = 0
$jobs_updated = 0

def process_files
    Dir.glob("**/*.md") do |filename|
        next if filename == '.' or filename == '..'
        next if !filename.downcase.include? "job-ads" # Only process of MD files in job ad folders
        job_data = get_values_from_markdown filename
        prepare_post_to_gh(job_data)
    end
end

def get_values_from_markdown(filename)
    puts "-" * 100
    puts "Parsing data from #{filename}".green

    parsed = FrontMatterParser::Parser.parse_file(filename)
    job_values = parsed.front_matter.transform_keys(&:to_sym)
    
    # Complain about unexpected values. This will help people troubleshoot faster
    if job_values.count != job_values.slice(:title,:location,:post_ids,:sync).count
        puts "Bad values in heading of file. Please ensure only 'title', 'location', 'post_ids', and 'sync' are used".red
        puts "Values found are #{job_values.keys}"
        $exit_code = 2
    end

    job_description_html = Kramdown::Document.new(parsed.content).to_html

    job_values[:filename] = filename
    job_values[:content] = job_description_html # Remove line breaks for GH API

    job_values
end

def set_greenhouse_headers(request)
    auth_token = Base64.encode64("#{GH_API_TOKEN}:")
    request["On-Behalf-Of"] = USER_ID.to_s
    request["Authorization"] = "Basic #{auth_token.strip}"

end

def prepare_post_to_gh (job_data)
    return if job_data.nil?
   
    puts "Processing: #{job_data[:filename]}".green
    post_ids = job_data[:post_ids]
    data_to_send = job_data.slice(:location, :title, :content)

    if job_data[:sync] != true 
        puts "\tNot syncing job posts '#{post_ids}' since job is not set to sync=true".red
        return
    end

    post_ids.to_s.split(',').each do |post_id|
        id = post_id.strip.to_i.to_s
        if id == "0" 
            puts "\tInvalid post id #{post_id}. Going to next post"
            $exit_code = 2
            next 
        end

        puts "\tUpdating job post #{post_id}"
        uri = URI.parse("https://harvest.greenhouse.io/v1/job_posts/#{post_id}")


        # Get the original job ad
        original_jobad_request = Net::HTTP::Get.new(uri)
        set_greenhouse_headers(original_jobad_request)
        original_ad_response = Net::HTTP.start(uri.hostname, uri.port, REQ_OPTIONS) do |http|
            http.request(original_jobad_request)
        end

        if original_ad_response.code.to_i == 200
            original_jobpost_data = JSON.parse(original_ad_response.body)
            internal = original_jobpost_data["internal"] == true

            # Check to see if the original job ad content is different from the content to be posted.
            # The GH API returns content in the 'internal_content' or 'content' field depending on job type. 
            if internal && original_jobpost_data["internal_content"] != data_to_send[:content]
                puts "\tUpdating internal job..."
                update_job(uri, data_to_send)
            elsif !internal && original_jobpost_data["content"] != data_to_send[:content]
                puts "\tUpdating external job..."
                update_job(uri, data_to_send)
            else
                puts "\tNo content has changed for this job. Skipping..."
            end
        else
            puts "Unable to retrieve job ad #{post_id}. Status code: #{original_ad_response.code}. Skipping...".red
            $exit_code = 4
        end  
    end
   
end

def update_job(uri, data)
    # The greenhouse API expect the JSON value for the job description to be in 'content' regardless of whether 
    # it is for an internal job post and will be eventually be stored in "internal_content" or an external job post 
    # and will be stored in "content".  GH takes care of setting internal_content. 
    # We just need to send in "content" and GH takes care of the rest. 

    patch_request = Net::HTTP::Patch.new(uri)
    patch_request.content_type = "application/json"

    set_greenhouse_headers(patch_request)    

    patch_request.body = data.to_json

    puts "\tJob post updated with title: '#{data[:title]}', location: '#{data[:location]}'."
    puts "\tJob description: #{data[:content][0...200]}"
    puts ""
    
    if !DRY_RUN
        response = Net::HTTP.start(uri.hostname, uri.port, REQ_OPTIONS) do |http|
            http.request(patch_request)
        end
        if response.code.to_i == 200
            puts "Job updated successfully.".green
            $jobs_updated = $jobs_updated + 1
        else
            puts "Invalid credentials or url. Check the $GH_USER_ID".red if response.code.to_i == 404
            puts "Error occurred. #{response.code} #{response.body}".red if response.code.to_i != 404 
            $exit_code = 5
        end
    else
        puts "Dry run...skipping posting to Greenhouse"
    end
end


def check_setup_values
    puts "Checking setup values and environment variables...."
    # Test API Token and complain if we can't access GH
    if ENV['GH_USER_ID'].nil? || ENV['GH_API_TOKEN'].nil?
        puts "Please set $GH_USER_ID".red if ENV['GH_USER_ID'].nil?
        puts "Please set $GH_API_TOKEN".red if ENV['GH_API_TOKEN'].nil?
        $exit_code = 100
    end 

    # Test out the tokens to make sure we can authenticate
    uri = URI.parse("https://harvest.greenhouse.io/v1/job_posts?per_page=1&page=1")       
    request = Net::HTTP::Get.new(uri)
    set_greenhouse_headers(request)
    response = Net::HTTP.start(uri.hostname, uri.port, REQ_OPTIONS) do |http|
        http.request(request)
    end

    if response.code.to_i == 200
        puts "Successfully authenticated to Greenhouse.".green 
    else
        puts "Error occurred connecting to Greenhouse. Error code: #{response.code}, Response: #{response.body[0...200]}".red
        $exit_code = 1
    end  
    
    puts "All updates in GH will be attributed to user id: #{ENV['GH_USER_ID']}. It is recommended that the account should be a non-interactive user account.".yellow

    if DRY_RUN
        puts "This is dry run. Job updates will not be posted to Greenhouse.".yellow       
    end
end

def output_final_message
    puts "#{$jobs_updated} total jobs updated.".green
    exit $exit_code
end

check_setup_values
process_files
output_final_message

# TODO: Introduce a mode to only loop through changed .md files in the most recent commit or to loop through all markdown files. 
# Todo: setup a "MR" mode in which files are not synced, but instead just validated for correctness. 
